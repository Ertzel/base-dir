'use strict';

var dir_name = __dirname.toString().replace(/\\node_modules\\base-dir/g, '');

module.exports = {
  root: dir_name,
  get base() {
    return this.root + '/';
  },
  path: function(dir, full) {
    if (full === undefined || full === null)
      full = true;
    if (full)
      return this.base + dir;
    else
      return './' + dir;
  },
  import: function (dir) {
    return require(this.base + dir);
  }
};