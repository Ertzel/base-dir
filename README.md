Get to the base directory of a project for importing files

## Install
```sh
$ npm install base-dir
```

## Basic Usage
```sh
// firstDir/myFile.js
module.exports = {
  text: 'hello-world'
};

// secondDir/main.js
var dir = require('base-dir');
var myFile = dir.import('firstDir/myFile.js');
console.log('text: %s', myFile.text);
```

## Setting root directory
```sh
// index.js
var dir = require('base-dir');
dir.root = 'src/';
var main = dir.import('secondDir/main.js');
console.log('text: %s', main.text);

// src/firstDir/myFile.js
module.exports = {
  text: 'hello-world'
};

// src/secondDir/main.js
var dir = require('base-dir');
var myFile = dir.import('firstDir/myFile.js');
module.exports = {
  text: myFile.text
};
```